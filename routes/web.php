<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Home');
})->name('home');

Route::get('empresa', function () {
    return view ('Empresa');
})->name('empresa');

Route::get('servico', function () {
    return view('Servicos');
})->name('servico');

Route::get('contato', function () {
    return view('Contato');
})->name('contato');
