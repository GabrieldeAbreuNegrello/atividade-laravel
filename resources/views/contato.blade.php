<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Contato</title>
</head>
<body>
        <ul>
            <li><a href="{{ route('home')}}">Home</a></li>
            <li><a href="{{ route('empresa')}}">Empresa</a></li>
            <li><a href="{{ route('servico')}}">Serviços</a></li>
            <li><a href="{{ route('contato')}}">Contato</a></li>

        </ul>
        
        <h1>Contato</h1>

        <h3>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas velit fugit sapiente corporis. Eius, vel voluptates doloremque ratione exercitationem itaque recusandae! Suscipit officiis, eum nesciunt repellendus aliquid ex placeat ipsa.</h3>

        <div>
            <label for="Name">Nome</label>
            <input type="text" id="Name" placeholder="Nome Completo">
        </div>

        <div>
            <label for="Email">Email</label>
            <input type="email" id="Email" placeholder="exemplo@gmail.com">
        </div>

        <div>
            <label for="Mensagem">Mensagem</label>
            <textarea id="Mensagem" rows="4"></textarea>
        </div>

        <div>
            <button>Enviar</button>
        </div>
        
</body>
</html>